# Test Flipper

I remember seeing on a youtube video a while ago someone's idea, which is implemented here. In each test, flip every `expect(...)` to `expect(...).not`, and vice versa. The `reverse-flip` file puts all `.bak` files that were created during the conversion back to their original place.

This can help people find out if there are any tests that do nothing, giving you the opportunity to either make the tests actually test what they are supposed to, or delete them altogether.

```sh
# For all files in `path/to/tests` that match glob `**/*.test.js*`, flip the
# expects and put the original files in `$file.bak`.
./test-flipper.sh /path/to/tests '**/*.test.js*'
# Revert all `$file.bak` files back to `$file`.
./test-flipper.sh /path/to/tests '**/*.test.js*'
```
