#!/usr/bin/env bash

_path="$1"

if [[ -z $_path ]]; then
  echo "Provide a path (e.g. \`./test\` or \`/tmp/files\`)."
  exit 1
fi

# Find all files in _path using glob
FILES="$(find "$_path" -path "**/*.bak")"

echo "$FILES" | while read -r file; do
  mv "$file" "${file%*\.bak}"
done
