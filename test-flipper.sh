#!/usr/bin/env bash

_path="$1"
glob="${2:-**/*}"

if [[ -z $_path ]]; then
  echo "Provide a path (e.g. \`./test\` or \`/tmp/files\`)."
  exit 1
elif [[ -z $glob ]]; then
  echo "No glob provided. Applying to all files in '$_path'."
fi

# Find all files in _path using glob
FILES="$(find "$_path" -path "$glob")"

echo "$FILES" | while read -r file; do
  # This incantation inside the `expect(...)` is a way to capture everything inside
  # including any parentheses groups. e.g. `expect(fn())` will still work.
  # https://stackoverflow.com/a/36435984
  # -0777 reads the whole file as a single string. Useful for multiline `expect` calls.
  perl -0777 -i.bak -pe '
    s/expect\(([^()]*+(?:\((?1)\)[^()]*)*+)\).not/~~NEW_TRUE~~(\1)~~NEW_TRUE~~/g;
    s/expect\(([^()]*+(?:\((?1)\)[^()]*)*+)\)/expect(\1).not/g;
    s/~~NEW_TRUE~~\(([^()]*+(?:\((?1)\)[^()]*)*+)\)~~NEW_TRUE~~/expect(\1)/g;
  ' "$file"
done
